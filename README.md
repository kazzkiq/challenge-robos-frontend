# ROBOS.im Challenge

**Note: This is just the front-end part of the project. You still need the back-end running in order to run the example. [You can get is here](https://bitbucket.org/kazzkiq/challenge-robos-backend).**



**The Telegram Bot is: `@challenge_robos_claudio_bot`**

### Instructions:

Make sure you have Node.js and NPM installed.

To run the frontend, first clone this repo, then:

1. Run `npm install -g brunch` (build-tool);
2. Run `npm install`;
3. Run `npm start`;

A server will start running at: `http://localhost:3333/`. Open it in browser.

For basic tests, run: `npm run test`.

This project uses:

- Babel;
- Inferno.js;
- Socket.io;
- Sass (SCSS);
- Viewports (micro-lib for responsiveness);
- Bulma (CSS Framework).