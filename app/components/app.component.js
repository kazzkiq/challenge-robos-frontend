export default function App({ children }) {
  return (
    <div className={'AppComponent'}>
      { children }
    </div>
  );
}
