import io from 'socket.io-client';
import Request from '../helpers/Request.helper';

export default class ChatDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.socket = io('http://localhost:3000');
    this.xhr = new Request();
    this.state = {
      messages: [],
    };
  }

  componentDidMount() {
    this.xhr.get(`/chats/${this.props.chatId}`).then((res) => {
      this.setState({ messages: res });
      this.scrollBottom();
      this.connectSocket();
    }).catch((err) => {
      console.log(err);
    });
  }

  connectSocket() {
    this.socket.on('server:message', (msg) => {

      // Message from another chat, so ignore in this window
      if (+msg.chat_id !== +this.props.chatId) {
        return;
      }

      this.pushToMessages(msg);
    });
  }

  pushToMessages(msg) {
    // Defining new message for visual effects
    msg.isNew = true;

    // Updating component state
    const messages = this.state.messages.slice();
    messages.push(msg);
    this.setState({
      messages,
    });

    // Scroll to bottom of chat
    this.scrollBottom();
  }

  scrollBottom() {
    this.chatRoom.scrollTop = this.chatRoom.scrollHeight;
  }

  sendMessage() {
    const message = document.getElementById('msgInput').value;
    const messageObj = {
      chat_id: this.props.chatId,
      message,
      username: 'challenge_robos_claudio_bot',
      date: +new Date(),
    };

    // Sending message to server via websockets
    this.socket.emit('client:message', messageObj);

    // Updating component (UI) with new message
    this.pushToMessages(messageObj);

    // Cleaning input to receive new messages
    document.getElementById('msgInput').value = '';
  }

  formatDate(value) {
    // Convert to Brasilia's timezone 
    const datetime = new Date(value - (1000 * 60 * 60 * 3)).toISOString();
    const date = datetime.substr(0, 10).split('-').reverse().join('/');
    const time = datetime.substr(11, 5);

    return `${date} ${time}`;
  }

  handleChatInput(event) {
    // If ENTER key pressed in input, trigger the message action
    if (event.keyCode === 13) {
      this.sendMessage();
    }
  }

  render() {
    return (
      <div className={'ChatDetailsComponent'} ref={(chatRoom) => { this.chatRoom = chatRoom; }}>
        {this.state.messages.map(message => (
          <div className={`app-msg-balloon${message.isNew ? ' animated rubberBand' : ''}${message.username !== 'challenge_robos_claudio_bot' ? '' : ' me'}`}>
              <p>{message.message}</p>
              <small><i className="fa fa-clock-o" aria-hidden="true"></i> {this.formatDate(message.date)}</small>
            </div>
          ))}

        <div className="chat-input">
          <div className="field has-addons">
            <p className="control">
              <input className="input" type="text" placeholder="Message here..." id="msgInput" onKeyDown={this.handleChatInput.bind(this)} />
            </p>
            <p className="control">
              <button className="button is-info" onClick={this.sendMessage.bind(this)}>
                Send Message
              </button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}
