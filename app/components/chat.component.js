import Request from '../helpers/Request.helper';
import Topbar from './topbar.component';
import Menu from './menu.component';
import ChatDetails from './chat-details.component';

export default class Chat extends Component {
  constructor(props, context) {
    super(props, context);
    this.xhr = new Request();
  }

  render() {
    return (
      <div className={'ChatComponent'}>
        <Topbar />
        <div className="columns is-mobile">
          <div className="column fixed-200">
            <Menu />
          </div>
          <div className="column chat-window">
            {(this.props.params.id) ? <ChatDetails chatId={this.props.params.id} /> : <div className="center-status">No chats opened.</div>}
          </div>
        </div>
      </div>
    );
  }
}
