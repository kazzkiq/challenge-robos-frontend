import Auth from '../helpers/Auth.helper';

export default function Login() {
  return (
    <div className={'LoginComponent'}>
      <h1 className="title">Bot Admin</h1>
      <h2 className="subtitle">To continue, please login with your Facebook account:</h2>
      <button onClick={Auth.login} className="button is-info">
        <span className="icon">
          <i className="fa fa-facebook"></i>
        </span>
        <span>Login with Facebook</span>
      </button>
    </div>
  );
}
