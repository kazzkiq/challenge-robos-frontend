import io from 'socket.io-client';
import Request from '../helpers/Request.helper';
import Auth from '../helpers/Auth.helper';

export default class Menu extends Component {
  constructor(props, context) {
    super(props, context);
    this.socket = io('http://localhost:3000');
    this.xhr = new Request();
    this.state = {
      chats: [],
    };
  }

  componentDidMount() {
    // Populate menu
    this.xhr.get('/chats').then((res) => {

      // If server returns auth error, logout imediatelly.
      if (res.access === 'NOT AUTHORIZED') {
        Auth.logout();
        return;
      }

      // Else, update menu with conversation list
      this.setState({ chats: res });

      // And start listening to new conversations
      this.connectSocket();
    }).catch((err) => {
      console.log(err);
    });
  }

  connectSocket() {

    this.socket.on('server:chat', (msg) => {
      // If new chat detected, push it to menu
      this.pushToMenu(msg);
    });
  }

  pushToMenu(msg) {
    this.setState({
      chats: msg,
    });
  }

  render() {
    return (
      <aside className="menu">
        <p className="menu-label">Chats</p>
        <ul className="menu-list">
          {this.state.chats.map(chat => (
            <li>
              <a href={`/chat/${chat._id.chat_id}`}>{chat._id.first_name}</a>
            </li>
          ))}
        </ul>
      </aside>
    );
  }
}
