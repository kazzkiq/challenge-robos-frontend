import Auth from '../helpers/Auth.helper';

export default function Topbar() {
  return (
    <nav className="nav">
      <div className="nav-left">
        <h1 className="nav-item">Bot Admin</h1>
      </div>
      <div className="nav-right">
        <div className="nav-item">
          <button className="button is-info" onClick={Auth.logout}>Logout</button>
        </div>
      </div>
    </nav>
  );
}
