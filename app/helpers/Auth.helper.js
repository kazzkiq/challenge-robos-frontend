export default class Auth {
  static isUserLogged() {
    const token = Auth.getUserToken();
    if (!token) {
      return false;
    }
    return true;
  }

  static getUserToken() {
    return sessionStorage.getItem('robos-fbAccessToken') || '';
  }

  static setUserToken(token) {
    sessionStorage.setItem('robos-fbAccessToken', token);
    return Auth.getUserToken();
  }

  static flushUserToken(token) {
    sessionStorage.removeItem('robos-fbAccessToken', token);
    if(Auth.getUserToken() === '') {
      return null;
    } else {
      return '';
    }
  }

  static login() {
    window.FB.login((response) => {
      if (response.authResponse) {
        Auth.setUserToken(response.authResponse.accessToken);
        window.location.href = '/chat';
      }
    });
  }

  static logout() {
    Auth.flushUserToken();
    window.location.reload();
  }
}
