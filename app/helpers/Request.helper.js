import axios from 'axios';
import Auth from './Auth.helper';

export default class Request {
  constructor() {
    this.axios = axios.create({
      baseURL: 'http://localhost:3000/',
      timeout: 30 * 1000, // 30 seconds
      headers: { 'FB-Login-Token': Auth.getUserToken() },
    });
  }

  get(path) {
    return new Promise((resolve, reject) => {
      this.axios.get(path)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
      });
    });
  }

  static getRemote(path) {
    return new Promise((resolve, reject) => {
      axios.get(path)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
      });
    });
  }
}
