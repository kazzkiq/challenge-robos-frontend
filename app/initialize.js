import { Router, Route, Redirect } from 'inferno-router';
import createBrowserHistory from 'history/createBrowserHistory';

import App from './components/app.component';
import Login from './components/login.component';
import Chat from './components/chat.component';
import ChatDetails from './components/chat-details.component';
import Auth from './helpers/Auth.helper';

const browserHistory = createBrowserHistory();

function authorizedOnly({ props, router }) {
  if (!Auth.isUserLogged()) {
    router.push('/login');
  }
}

const routes = (
  <Router history={browserHistory}>
    <Route component={App}>
      <Redirect from="/" to="/chat" />
      <Route path="chat" component={Chat} onEnter={authorizedOnly}>
        <Route path="/chat/:id" component={ChatDetails} />
      </Route>
      <Route path="*" component={Login} />
    </Route>
  </Router>
);

function main() {
  // Once FB API loads...
  window.handleFBLogin = () => {
    // Start application
    Inferno.render(routes, document.getElementById('app'));
  };
}

document.addEventListener('DOMContentLoaded', main);
