import { assert } from 'chai';
import Auth from '../app/helpers/Auth.helper';

describe('Auth', () => {
  describe('#isUserLogged', () => {
    it('should always return string', () => {
      assert.isBoolean(Auth.isUserLogged());
    });
  });
  describe('#getUserToken', () => {
    it('should always return string', () => {
      assert.isString(Auth.getUserToken());
    });
  });
  describe('#setUserToken', () => {
    it('should return value sent', () => {
      assert.equal(Auth.setUserToken('abc'), 'abc');
    });
  });
  describe('#flushUserToken', () => {
    it('should return null', () => {
      assert.isNull(Auth.flushUserToken());
    });
  });
});